<?php

/**
 * @file
 * Contains \Drupal\clear_cache_button\Form\ClearCacheForm.
 */

namespace Drupal\clear_cache_button\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

class ClearCacheForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'clear_cache_form';
  }

  /**
   * {@inheritdoc}.
   */
   public function buildForm(array $form, FormStateInterface $form_state) {

     $form['clear_cache']['clear'] = array(
       '#type' => 'submit',
       '#value' => t('Clear all caches'),
     );

     return $form;
   }

   /**
    * {@inheritdoc}.
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      drupal_flush_all_caches();
      drupal_set_message(t('Caches cleared.'));
    }
}
