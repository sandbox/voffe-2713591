<?php

 /**
  * Provides a 'Clear cache button' Block
  *
  * @Block(
  *   id = "clear_cache_button",
  *   admin_label = @Translation("Clear cache button block"),
  * )
  */
namespace Drupal\clear_cache_button\Plugin\Block;

use Drupal\Core\Block\BlockBase;

class ClearCacheBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\clear_cache_button\Form\ClearCacheForm');

    return $form;
  }

}
